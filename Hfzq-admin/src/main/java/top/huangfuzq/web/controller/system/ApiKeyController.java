package top.huangfuzq.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.huangfuzq.common.annotation.Log;
import top.huangfuzq.common.core.controller.BaseController;
import top.huangfuzq.common.core.domain.AjaxResult;
import top.huangfuzq.common.enums.BusinessType;
import top.huangfuzq.system.domain.ApiKey;
import top.huangfuzq.system.service.IApiKeyService;
import top.huangfuzq.common.utils.poi.ExcelUtil;
import top.huangfuzq.common.core.page.TableDataInfo;

/**
 * 用于存储API键信息的数据Controller
 *
 * @author hfzq
 * @date 2023-03-17
 */
@RestController
@RequestMapping("/system/key")
public class ApiKeyController extends BaseController
{
    @Autowired
    private IApiKeyService apiKeyService;

    /**
     * 查询用于存储API键信息的数据列表
     */
    @PreAuthorize("@ss.hasPermi('system:key:list')")
    @GetMapping("/list")
    public TableDataInfo list(ApiKey apiKey)
    {
        startPage();
        List<ApiKey> list = apiKeyService.selectApiKeyList(apiKey);
        return getDataTable(list);
    }

    /**
     * 导出用于存储API键信息的数据列表
     */
    @PreAuthorize("@ss.hasPermi('system:key:export')")
    @Log(title = "用于存储API键信息的数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ApiKey apiKey)
    {
        List<ApiKey> list = apiKeyService.selectApiKeyList(apiKey);
        ExcelUtil<ApiKey> util = new ExcelUtil<ApiKey>(ApiKey.class);
        util.exportExcel(response, list, "用于存储API键信息的数据数据");
    }

    /**
     * 获取用于存储API键信息的数据详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:key:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(apiKeyService.selectApiKeyById(id));
    }

    /**
     * 新增用于存储API键信息的数据
     */
    @PreAuthorize("@ss.hasPermi('system:key:add')")
    @Log(title = "用于存储API键信息的数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ApiKey apiKey)
    {
        return toAjax(apiKeyService.insertApiKey(apiKey));
    }

    /**
     * 修改用于存储API键信息的数据
     */
    @PreAuthorize("@ss.hasPermi('system:key:edit')")
    @Log(title = "用于存储API键信息的数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ApiKey apiKey)
    {
        return toAjax(apiKeyService.updateApiKey(apiKey));
    }

    /**
     * 删除用于存储API键信息的数据
     */
    @PreAuthorize("@ss.hasPermi('system:key:remove')")
    @Log(title = "用于存储API键信息的数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(apiKeyService.deleteApiKeyByIds(ids));
    }
}
