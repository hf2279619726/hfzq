package top.huangfuzq.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.huangfuzq.common.annotation.Log;
import top.huangfuzq.common.core.controller.BaseController;
import top.huangfuzq.common.core.domain.AjaxResult;
import top.huangfuzq.common.enums.BusinessType;
import top.huangfuzq.system.domain.DeviceProfiles;
import top.huangfuzq.system.service.IDeviceProfilesService;
import top.huangfuzq.common.utils.poi.ExcelUtil;
import top.huangfuzq.common.core.page.TableDataInfo;

/**
 * 存储设备概要文件的详细信息Controller
 *
 * @author hfzq
 * @date 2023-03-17
 */
@RestController
@RequestMapping("/system/profiles")
public class DeviceProfilesController extends BaseController
{
    @Autowired
    private IDeviceProfilesService deviceProfilesService;

    /**
     * 查询存储设备概要文件的详细信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:profiles:list')")
    @GetMapping("/list")
    public TableDataInfo list(DeviceProfiles deviceProfiles)
    {
        startPage();
        List<DeviceProfiles> list = deviceProfilesService.selectDeviceProfilesList(deviceProfiles);
        return getDataTable(list);
    }

    /**
     * 导出存储设备概要文件的详细信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:profiles:export')")
    @Log(title = "存储设备概要文件的详细信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DeviceProfiles deviceProfiles)
    {
        List<DeviceProfiles> list = deviceProfilesService.selectDeviceProfilesList(deviceProfiles);
        ExcelUtil<DeviceProfiles> util = new ExcelUtil<DeviceProfiles>(DeviceProfiles.class);
        util.exportExcel(response, list, "存储设备概要文件的详细信息数据");
    }

    /**
     * 获取存储设备概要文件的详细信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:profiles:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(deviceProfilesService.selectDeviceProfilesById(id));
    }

    /**
     * 新增存储设备概要文件的详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:profiles:add')")
    @Log(title = "存储设备概要文件的详细信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DeviceProfiles deviceProfiles)
    {
        return toAjax(deviceProfilesService.insertDeviceProfiles(deviceProfiles));
    }

    /**
     * 修改存储设备概要文件的详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:profiles:edit')")
    @Log(title = "存储设备概要文件的详细信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DeviceProfiles deviceProfiles)
    {
        return toAjax(deviceProfilesService.updateDeviceProfiles(deviceProfiles));
    }

    /**
     * 删除存储设备概要文件的详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:profiles:remove')")
    @Log(title = "存储设备概要文件的详细信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(deviceProfilesService.deleteDeviceProfilesByIds(ids));
    }
}
