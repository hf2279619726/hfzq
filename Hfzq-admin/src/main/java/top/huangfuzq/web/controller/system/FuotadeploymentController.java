package top.huangfuzq.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.huangfuzq.common.annotation.Log;
import top.huangfuzq.common.core.controller.BaseController;
import top.huangfuzq.common.core.domain.AjaxResult;
import top.huangfuzq.common.enums.BusinessType;
import top.huangfuzq.system.domain.Fuotadeployment;
import top.huangfuzq.system.service.IFuotadeploymentService;
import top.huangfuzq.common.utils.poi.ExcelUtil;
import top.huangfuzq.common.core.page.TableDataInfo;

/**
 * 无线固件更新部署Controller
 *
 * @author hfzq
 * @date 2023-03-17
 */
@RestController
@RequestMapping("/system/fuotadeployment")
public class FuotadeploymentController extends BaseController
{
    @Autowired
    private IFuotadeploymentService fuotadeploymentService;

    /**
     * 查询无线固件更新部署列表
     */
    @PreAuthorize("@ss.hasPermi('system:fuotadeployment:list')")
    @GetMapping("/list")
    public TableDataInfo list(Fuotadeployment fuotadeployment)
    {
        startPage();
        List<Fuotadeployment> list = fuotadeploymentService.selectFuotadeploymentList(fuotadeployment);
        return getDataTable(list);
    }

    /**
     * 导出无线固件更新部署列表
     */
    @PreAuthorize("@ss.hasPermi('system:fuotadeployment:export')")
    @Log(title = "无线固件更新部署", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Fuotadeployment fuotadeployment)
    {
        List<Fuotadeployment> list = fuotadeploymentService.selectFuotadeploymentList(fuotadeployment);
        ExcelUtil<Fuotadeployment> util = new ExcelUtil<Fuotadeployment>(Fuotadeployment.class);
        util.exportExcel(response, list, "无线固件更新部署数据");
    }

    /**
     * 获取无线固件更新部署详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:fuotadeployment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(fuotadeploymentService.selectFuotadeploymentById(id));
    }

    /**
     * 新增无线固件更新部署
     */
    @PreAuthorize("@ss.hasPermi('system:fuotadeployment:add')")
    @Log(title = "无线固件更新部署", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Fuotadeployment fuotadeployment)
    {
        return toAjax(fuotadeploymentService.insertFuotadeployment(fuotadeployment));
    }

    /**
     * 修改无线固件更新部署
     */
    @PreAuthorize("@ss.hasPermi('system:fuotadeployment:edit')")
    @Log(title = "无线固件更新部署", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Fuotadeployment fuotadeployment)
    {
        return toAjax(fuotadeploymentService.updateFuotadeployment(fuotadeployment));
    }

    /**
     * 删除无线固件更新部署
     */
    @PreAuthorize("@ss.hasPermi('system:fuotadeployment:remove')")
    @Log(title = "无线固件更新部署", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(fuotadeploymentService.deleteFuotadeploymentByIds(ids));
    }
}
