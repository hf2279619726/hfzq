package top.huangfuzq.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.huangfuzq.common.annotation.Log;
import top.huangfuzq.common.core.controller.BaseController;
import top.huangfuzq.common.core.domain.AjaxResult;
import top.huangfuzq.common.enums.BusinessType;
import top.huangfuzq.system.domain.Gateway;
import top.huangfuzq.system.service.IGatewayService;
import top.huangfuzq.common.utils.poi.ExcelUtil;
import top.huangfuzq.common.core.page.TableDataInfo;

/**
 * 网关信息Controller
 *
 * @author hfzq
 * @date 2023-03-17
 */
@RestController
@RequestMapping("/system/gateway")
public class GatewayController extends BaseController
{
    @Autowired
    private IGatewayService gatewayService;

    /**
     * 查询网关信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:gateway:list')")
    @GetMapping("/list")
    public TableDataInfo list(Gateway gateway)
    {
        startPage();
        List<Gateway> list = gatewayService.selectGatewayList(gateway);
        return getDataTable(list);
    }

    /**
     * 导出网关信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:gateway:export')")
    @Log(title = "网关信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Gateway gateway)
    {
        List<Gateway> list = gatewayService.selectGatewayList(gateway);
        ExcelUtil<Gateway> util = new ExcelUtil<Gateway>(Gateway.class);
        util.exportExcel(response, list, "网关信息数据");
    }

    /**
     * 获取网关信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:gateway:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(gatewayService.selectGatewayById(id));
    }

    /**
     * 新增网关信息
     */
    @PreAuthorize("@ss.hasPermi('system:gateway:add')")
    @Log(title = "网关信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Gateway gateway)
    {
        return toAjax(gatewayService.insertGateway(gateway));
    }

    /**
     * 修改网关信息
     */
    @PreAuthorize("@ss.hasPermi('system:gateway:edit')")
    @Log(title = "网关信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Gateway gateway)
    {
        return toAjax(gatewayService.updateGateway(gateway));
    }

    /**
     * 删除网关信息
     */
    @PreAuthorize("@ss.hasPermi('system:gateway:remove')")
    @Log(title = "网关信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(gatewayService.deleteGatewayByIds(ids));
    }
}
