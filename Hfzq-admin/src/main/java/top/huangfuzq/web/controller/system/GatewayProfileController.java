package top.huangfuzq.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.huangfuzq.common.annotation.Log;
import top.huangfuzq.common.core.controller.BaseController;
import top.huangfuzq.common.core.domain.AjaxResult;
import top.huangfuzq.common.enums.BusinessType;
import top.huangfuzq.system.domain.GatewayProfile;
import top.huangfuzq.system.service.IGatewayProfileService;
import top.huangfuzq.common.utils.poi.ExcelUtil;
import top.huangfuzq.common.core.page.TableDataInfo;

/**
 * 网关概要配置Controller
 *
 * @author hfzq
 * @date 2023-03-17
 */
@RestController
@RequestMapping("/system/profile")
public class GatewayProfileController extends BaseController
{
    @Autowired
    private IGatewayProfileService gatewayProfileService;

    /**
     * 查询网关概要配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:profile:list')")
    @GetMapping("/list")
    public TableDataInfo list(GatewayProfile gatewayProfile)
    {
        startPage();
        List<GatewayProfile> list = gatewayProfileService.selectGatewayProfileList(gatewayProfile);
        return getDataTable(list);
    }

    /**
     * 导出网关概要配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:profile:export')")
    @Log(title = "网关概要配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, GatewayProfile gatewayProfile)
    {
        List<GatewayProfile> list = gatewayProfileService.selectGatewayProfileList(gatewayProfile);
        ExcelUtil<GatewayProfile> util = new ExcelUtil<GatewayProfile>(GatewayProfile.class);
        util.exportExcel(response, list, "网关概要配置数据");
    }

    /**
     * 获取网关概要配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:profile:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(gatewayProfileService.selectGatewayProfileById(id));
    }

    /**
     * 新增网关概要配置
     */
    @PreAuthorize("@ss.hasPermi('system:profile:add')")
    @Log(title = "网关概要配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GatewayProfile gatewayProfile)
    {
        return toAjax(gatewayProfileService.insertGatewayProfile(gatewayProfile));
    }

    /**
     * 修改网关概要配置
     */
    @PreAuthorize("@ss.hasPermi('system:profile:edit')")
    @Log(title = "网关概要配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GatewayProfile gatewayProfile)
    {
        return toAjax(gatewayProfileService.updateGatewayProfile(gatewayProfile));
    }

    /**
     * 删除网关概要配置
     */
    @PreAuthorize("@ss.hasPermi('system:profile:remove')")
    @Log(title = "网关概要配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(gatewayProfileService.deleteGatewayProfileByIds(ids));
    }
}
