package top.huangfuzq.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.huangfuzq.common.annotation.Log;
import top.huangfuzq.common.core.controller.BaseController;
import top.huangfuzq.common.core.domain.AjaxResult;
import top.huangfuzq.common.enums.BusinessType;
import top.huangfuzq.system.domain.Waterrecord;
import top.huangfuzq.system.service.IWaterrecordService;
import top.huangfuzq.common.utils.poi.ExcelUtil;
import top.huangfuzq.common.core.page.TableDataInfo;

/**
 * 设备用水记录Controller
 * 
 * @author hfzq
 * @date 2023-03-17
 */
@RestController
@RequestMapping("/system/waterrecord")
public class WaterrecordController extends BaseController
{
    @Autowired
    private IWaterrecordService waterrecordService;

    /**
     * 查询设备用水记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:waterrecord:list')")
    @GetMapping("/list")
    public TableDataInfo list(Waterrecord waterrecord)
    {
        startPage();
        List<Waterrecord> list = waterrecordService.selectWaterrecordList(waterrecord);
        return getDataTable(list);
    }

    /**
     * 导出设备用水记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:waterrecord:export')")
    @Log(title = "设备用水记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Waterrecord waterrecord)
    {
        List<Waterrecord> list = waterrecordService.selectWaterrecordList(waterrecord);
        ExcelUtil<Waterrecord> util = new ExcelUtil<Waterrecord>(Waterrecord.class);
        util.exportExcel(response, list, "设备用水记录数据");
    }

    /**
     * 获取设备用水记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:waterrecord:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(waterrecordService.selectWaterrecordById(id));
    }

    /**
     * 新增设备用水记录
     */
    @PreAuthorize("@ss.hasPermi('system:waterrecord:add')")
    @Log(title = "设备用水记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Waterrecord waterrecord)
    {
        return toAjax(waterrecordService.insertWaterrecord(waterrecord));
    }

    /**
     * 修改设备用水记录
     */
    @PreAuthorize("@ss.hasPermi('system:waterrecord:edit')")
    @Log(title = "设备用水记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Waterrecord waterrecord)
    {
        return toAjax(waterrecordService.updateWaterrecord(waterrecord));
    }

    /**
     * 删除设备用水记录
     */
    @PreAuthorize("@ss.hasPermi('system:waterrecord:remove')")
    @Log(title = "设备用水记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(waterrecordService.deleteWaterrecordByIds(ids));
    }
}
