package top.huangfuzq.common.enums;

/**
 * 操作状态
 *
 * @author hfzq
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
