package top.huangfuzq.common.enums;

/**
 * 数据源
 *
 * @author hfzq
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
