package top.huangfuzq.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import top.huangfuzq.common.annotation.Excel;
import top.huangfuzq.common.core.domain.BaseEntity;

/**
 * 用于存储API键信息的数据对象 api_key
 * 
 * @author hfzq
 * @date 2023-03-17
 */
public class ApiKey extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** API键ID */
    private Long id;

    /** API键名称 */
    @Excel(name = "API键名称")
    private String name;

    /** 是否管理员 */
    @Excel(name = "是否管理员")
    private Integer isAdmin;

    /** 应用ID */
    @Excel(name = "应用ID")
    private String applicationId;

    /** 组织ID */
    @Excel(name = "组织ID")
    private String organizationId;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 最后更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setIsAdmin(Integer isAdmin) 
    {
        this.isAdmin = isAdmin;
    }

    public Integer getIsAdmin() 
    {
        return isAdmin;
    }
    public void setApplicationId(String applicationId) 
    {
        this.applicationId = applicationId;
    }

    public String getApplicationId() 
    {
        return applicationId;
    }
    public void setOrganizationId(String organizationId) 
    {
        this.organizationId = organizationId;
    }

    public String getOrganizationId() 
    {
        return organizationId;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("isAdmin", getIsAdmin())
            .append("applicationId", getApplicationId())
            .append("organizationId", getOrganizationId())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
