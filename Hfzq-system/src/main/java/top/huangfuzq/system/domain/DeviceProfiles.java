package top.huangfuzq.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import top.huangfuzq.common.annotation.Excel;
import top.huangfuzq.common.core.domain.BaseEntity;

/**
 * 存储设备概要文件的详细信息对象 device_profiles
 * 
 * @author hfzq
 * @date 2023-03-17
 */
public class DeviceProfiles extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 设备概要文件的唯一标识符 */
    private Long id;

    /** 设备概要文件的名称 */
    @Excel(name = "设备概要文件的名称")
    private String name;

    /** 组织的唯一标识符 */
    @Excel(name = "组织的唯一标识符")
    private String organizationid;

    /** 网络服务器的唯一标识符 */
    @Excel(name = "网络服务器的唯一标识符")
    private String networkserverid;

    /** payload 编解码类型 */
    @Excel(name = "payload 编解码类型")
    private String payloadcodec;

    /** 解码负载消息所需的脚本 */
    @Excel(name = "解码负载消息所需的脚本")
    private String payloaddecoderscript;

    /** 编码负载消息所需的脚本 */
    @Excel(name = "编码负载消息所需的脚本")
    private String payloadencoderscript;

    /** 设备的 LoRaWAN MAC 版本 */
    @Excel(name = "设备的 LoRaWAN MAC 版本")
    private String macversion;

    /** 设备是否支持 32 位帧计数器 */
    @Excel(name = "设备是否支持 32 位帧计数器")
    private Integer supports32bitfcnt;

    /** 设备是否支持 Class B */
    @Excel(name = "设备是否支持 Class B")
    private Integer supportsclassb;

    /** 设备是否支持 Class C */
    @Excel(name = "设备是否支持 Class C")
    private Integer supportsclassc;

    /** 设备是否支持 OTAA */
    @Excel(name = "设备是否支持 OTAA")
    private Integer supportsjoin;

    /** 设备收到接收窗口1的时间（秒） */
    @Excel(name = "设备收到接收窗口1的时间", readConverterExp = "秒=")
    private Long rxdelay1;

    /** 设备在接收窗口2收到数据的数据速率 */
    @Excel(name = "设备在接收窗口2收到数据的数据速率")
    private Long rxdatarate2;

    /** 设备在接收窗口2中的接收频率 */
    @Excel(name = "设备在接收窗口2中的接收频率")
    private Long rxfreq2;

    /** ADR算法的唯一标识符 */
    @Excel(name = "ADR算法的唯一标识符")
    private String adralgorithmid;

    /** 设备支持的最大发射功率 */
    @Excel(name = "设备支持的最大发射功率")
    private Long maxeirp;

    /** 设备的最大占空比 */
    @Excel(name = "设备的最大占空比")
    private Long maxdutycycle;

    /** 设备PING时隙的周期 */
    @Excel(name = "设备PING时隙的周期")
    private Long pingslotperiod;

    /** 设备PING时隙的频率 */
    @Excel(name = "设备PING时隙的频率")
    private Long pingslotfreq;

    /** 设备PING时隙的数据速率 */
    @Excel(name = "设备PING时隙的数据速率")
    private Long pingslotdr;

    /** 设备概要文件的创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "设备概要文件的创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdat;

    /** 设备概要文件的更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "设备概要文件的更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedat;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setOrganizationid(String organizationid) 
    {
        this.organizationid = organizationid;
    }

    public String getOrganizationid() 
    {
        return organizationid;
    }
    public void setNetworkserverid(String networkserverid) 
    {
        this.networkserverid = networkserverid;
    }

    public String getNetworkserverid() 
    {
        return networkserverid;
    }
    public void setPayloadcodec(String payloadcodec) 
    {
        this.payloadcodec = payloadcodec;
    }

    public String getPayloadcodec() 
    {
        return payloadcodec;
    }
    public void setPayloaddecoderscript(String payloaddecoderscript) 
    {
        this.payloaddecoderscript = payloaddecoderscript;
    }

    public String getPayloaddecoderscript() 
    {
        return payloaddecoderscript;
    }
    public void setPayloadencoderscript(String payloadencoderscript) 
    {
        this.payloadencoderscript = payloadencoderscript;
    }

    public String getPayloadencoderscript() 
    {
        return payloadencoderscript;
    }
    public void setMacversion(String macversion) 
    {
        this.macversion = macversion;
    }

    public String getMacversion() 
    {
        return macversion;
    }
    public void setSupports32bitfcnt(Integer supports32bitfcnt) 
    {
        this.supports32bitfcnt = supports32bitfcnt;
    }

    public Integer getSupports32bitfcnt() 
    {
        return supports32bitfcnt;
    }
    public void setSupportsclassb(Integer supportsclassb) 
    {
        this.supportsclassb = supportsclassb;
    }

    public Integer getSupportsclassb() 
    {
        return supportsclassb;
    }
    public void setSupportsclassc(Integer supportsclassc) 
    {
        this.supportsclassc = supportsclassc;
    }

    public Integer getSupportsclassc() 
    {
        return supportsclassc;
    }
    public void setSupportsjoin(Integer supportsjoin) 
    {
        this.supportsjoin = supportsjoin;
    }

    public Integer getSupportsjoin() 
    {
        return supportsjoin;
    }
    public void setRxdelay1(Long rxdelay1) 
    {
        this.rxdelay1 = rxdelay1;
    }

    public Long getRxdelay1() 
    {
        return rxdelay1;
    }
    public void setRxdatarate2(Long rxdatarate2) 
    {
        this.rxdatarate2 = rxdatarate2;
    }

    public Long getRxdatarate2() 
    {
        return rxdatarate2;
    }
    public void setRxfreq2(Long rxfreq2) 
    {
        this.rxfreq2 = rxfreq2;
    }

    public Long getRxfreq2() 
    {
        return rxfreq2;
    }
    public void setAdralgorithmid(String adralgorithmid) 
    {
        this.adralgorithmid = adralgorithmid;
    }

    public String getAdralgorithmid() 
    {
        return adralgorithmid;
    }
    public void setMaxeirp(Long maxeirp) 
    {
        this.maxeirp = maxeirp;
    }

    public Long getMaxeirp() 
    {
        return maxeirp;
    }
    public void setMaxdutycycle(Long maxdutycycle) 
    {
        this.maxdutycycle = maxdutycycle;
    }

    public Long getMaxdutycycle() 
    {
        return maxdutycycle;
    }
    public void setPingslotperiod(Long pingslotperiod) 
    {
        this.pingslotperiod = pingslotperiod;
    }

    public Long getPingslotperiod() 
    {
        return pingslotperiod;
    }
    public void setPingslotfreq(Long pingslotfreq) 
    {
        this.pingslotfreq = pingslotfreq;
    }

    public Long getPingslotfreq() 
    {
        return pingslotfreq;
    }
    public void setPingslotdr(Long pingslotdr) 
    {
        this.pingslotdr = pingslotdr;
    }

    public Long getPingslotdr() 
    {
        return pingslotdr;
    }
    public void setCreatedat(Date createdat) 
    {
        this.createdat = createdat;
    }

    public Date getCreatedat() 
    {
        return createdat;
    }
    public void setUpdatedat(Date updatedat) 
    {
        this.updatedat = updatedat;
    }

    public Date getUpdatedat() 
    {
        return updatedat;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("organizationid", getOrganizationid())
            .append("networkserverid", getNetworkserverid())
            .append("payloadcodec", getPayloadcodec())
            .append("payloaddecoderscript", getPayloaddecoderscript())
            .append("payloadencoderscript", getPayloadencoderscript())
            .append("macversion", getMacversion())
            .append("supports32bitfcnt", getSupports32bitfcnt())
            .append("supportsclassb", getSupportsclassb())
            .append("supportsclassc", getSupportsclassc())
            .append("supportsjoin", getSupportsjoin())
            .append("rxdelay1", getRxdelay1())
            .append("rxdatarate2", getRxdatarate2())
            .append("rxfreq2", getRxfreq2())
            .append("adralgorithmid", getAdralgorithmid())
            .append("maxeirp", getMaxeirp())
            .append("maxdutycycle", getMaxdutycycle())
            .append("pingslotperiod", getPingslotperiod())
            .append("pingslotfreq", getPingslotfreq())
            .append("pingslotdr", getPingslotdr())
            .append("createdat", getCreatedat())
            .append("updatedat", getUpdatedat())
            .toString();
    }
}
