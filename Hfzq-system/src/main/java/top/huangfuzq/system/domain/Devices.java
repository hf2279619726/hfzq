package top.huangfuzq.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import top.huangfuzq.common.annotation.Excel;
import top.huangfuzq.common.core.domain.BaseEntity;

/**
 * 设备管理对象 devices
 *
 * @author hfzq
 * @date 2023-03-17
 */
public class Devices extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 寝室 */
    @Excel(name = "寝室")
    private String dormitory;

    /** 设备号 */
    @Excel(name = "设备号")
    private String deveui;

    /** 状态(0==在线，1==使用中，-1==异常) */
    @Excel(name = "状态(0==在线，1==使用中，-1==异常)")
    private Integer status;

    /** 阀门编号 */
    @Excel(name = "阀门编号")
    private String valveNumber;

    /** 信号强度 */
    @Excel(name = "信号强度")
    private String signalStrength;

    /** 信噪比 */
    @Excel(name = "信噪比")
    private String signalToNoiseRatio;

    /** 数据创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "数据创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtCreate;

    /** 数据最后修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "数据最后修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtModified;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setDormitory(String dormitory)
    {
        this.dormitory = dormitory;
    }

    public String getDormitory()
    {
        return dormitory;
    }
    public void setDeveui(String deveui)
    {
        this.deveui = deveui;
    }

    public String getDeveui()
    {
        return deveui;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }
    public void setValveNumber(String valveNumber)
    {
        this.valveNumber = valveNumber;
    }

    public String getValveNumber()
    {
        return valveNumber;
    }
    public void setSignalStrength(String signalStrength)
    {
        this.signalStrength = signalStrength;
    }

    public String getSignalStrength()
    {
        return signalStrength;
    }
    public void setSignalToNoiseRatio(String signalToNoiseRatio)
    {
        this.signalToNoiseRatio = signalToNoiseRatio;
    }

    public String getSignalToNoiseRatio()
    {
        return signalToNoiseRatio;
    }
    public void setGmtCreate(Date gmtCreate)
    {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtCreate()
    {
        return gmtCreate;
    }
    public void setGmtModified(Date gmtModified)
    {
        this.gmtModified = gmtModified;
    }

    public Date getGmtModified()
    {
        return gmtModified;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dormitory", getDormitory())
            .append("deveui", getDeveui())
            .append("status", getStatus())
            .append("valveNumber", getValveNumber())
            .append("signalStrength", getSignalStrength())
            .append("signalToNoiseRatio", getSignalToNoiseRatio())
            .append("gmtCreate", getGmtCreate())
            .append("gmtModified", getGmtModified())
            .toString();
    }
}
