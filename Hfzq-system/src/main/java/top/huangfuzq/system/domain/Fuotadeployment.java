package top.huangfuzq.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import top.huangfuzq.common.annotation.Excel;
import top.huangfuzq.common.core.domain.BaseEntity;

/**
 * 无线固件更新部署对象 fuotadeployment
 * 
 * @author hfzq
 * @date 2023-03-17
 */
public class Fuotadeployment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 应用程序的唯一标识符 */
    private Long id;

    /** 应用程序的唯一标识符 */
    @Excel(name = "应用程序的唯一标识符")
    private String applicationid;

    /** 简介 */
    @Excel(name = "简介")
    private String description;

    /** 设备概要文件的唯一标识符 */
    @Excel(name = "设备概要文件的唯一标识符")
    private String deviceprofileid;

    /** 设备EUI */
    @Excel(name = "设备EUI")
    private String deveui;

    /** 名字 */
    @Excel(name = "名字")
    private Long name;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setApplicationid(String applicationid) 
    {
        this.applicationid = applicationid;
    }

    public String getApplicationid() 
    {
        return applicationid;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setDeviceprofileid(String deviceprofileid) 
    {
        this.deviceprofileid = deviceprofileid;
    }

    public String getDeviceprofileid() 
    {
        return deviceprofileid;
    }
    public void setDeveui(String deveui) 
    {
        this.deveui = deveui;
    }

    public String getDeveui() 
    {
        return deveui;
    }
    public void setName(Long name) 
    {
        this.name = name;
    }

    public Long getName() 
    {
        return name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("applicationid", getApplicationid())
            .append("description", getDescription())
            .append("deviceprofileid", getDeviceprofileid())
            .append("deveui", getDeveui())
            .append("name", getName())
            .toString();
    }
}
