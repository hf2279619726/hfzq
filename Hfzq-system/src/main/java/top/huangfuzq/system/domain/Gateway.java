package top.huangfuzq.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import top.huangfuzq.common.annotation.Excel;
import top.huangfuzq.common.core.domain.BaseEntity;

/**
 * 网关信息对象 gateway
 * 
 * @author hfzq
 * @date 2023-03-17
 */
public class Gateway extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 网关ID */
    private Long id;

    /** 网关名称 */
    @Excel(name = "网关名称")
    private String name;

    /** 网关描述 */
    @Excel(name = "网关描述")
    private String description;

    /** 组织ID */
    @Excel(name = "组织ID")
    private String organizationId;

    /** 关联网络服务器的ID */
    @Excel(name = "关联网络服务器的ID")
    private String networkServerId;

    /** 关联网关概要配置的ID */
    @Excel(name = "关联网关概要配置的ID")
    private String gatewayProfileId;

    /** 关联服务配置的ID */
    @Excel(name = "关联服务配置的ID")
    private String serviceProfileId;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 最后更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setOrganizationId(String organizationId) 
    {
        this.organizationId = organizationId;
    }

    public String getOrganizationId() 
    {
        return organizationId;
    }
    public void setNetworkServerId(String networkServerId) 
    {
        this.networkServerId = networkServerId;
    }

    public String getNetworkServerId() 
    {
        return networkServerId;
    }
    public void setGatewayProfileId(String gatewayProfileId) 
    {
        this.gatewayProfileId = gatewayProfileId;
    }

    public String getGatewayProfileId() 
    {
        return gatewayProfileId;
    }
    public void setServiceProfileId(String serviceProfileId) 
    {
        this.serviceProfileId = serviceProfileId;
    }

    public String getServiceProfileId() 
    {
        return serviceProfileId;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("description", getDescription())
            .append("organizationId", getOrganizationId())
            .append("networkServerId", getNetworkServerId())
            .append("gatewayProfileId", getGatewayProfileId())
            .append("serviceProfileId", getServiceProfileId())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
