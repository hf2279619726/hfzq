package top.huangfuzq.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import top.huangfuzq.common.annotation.Excel;
import top.huangfuzq.common.core.domain.BaseEntity;

/**
 * 网关概要配置对象 gateway_profile
 * 
 * @author hfzq
 * @date 2023-03-17
 */
public class GatewayProfile extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 网关概要配置ID */
    private Long id;

    /** 网关概要配置名称 */
    @Excel(name = "网关概要配置名称")
    private String name;

    /** 关联网络服务器的ID */
    @Excel(name = "关联网络服务器的ID")
    private Long networkServerId;

    /** 统计数据的间隔时间（单位：秒） */
    @Excel(name = "统计数据的间隔时间", readConverterExp = "单=位：秒")
    private Long statsInterval;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 最后更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    /** 附加通道（使用JSON格式） */
    @Excel(name = "附加通道", readConverterExp = "使=用JSON格式")
    private String extraChannels;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setNetworkServerId(Long networkServerId) 
    {
        this.networkServerId = networkServerId;
    }

    public Long getNetworkServerId() 
    {
        return networkServerId;
    }
    public void setStatsInterval(Long statsInterval) 
    {
        this.statsInterval = statsInterval;
    }

    public Long getStatsInterval() 
    {
        return statsInterval;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }
    public void setExtraChannels(String extraChannels) 
    {
        this.extraChannels = extraChannels;
    }

    public String getExtraChannels() 
    {
        return extraChannels;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("networkServerId", getNetworkServerId())
            .append("statsInterval", getStatsInterval())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .append("extraChannels", getExtraChannels())
            .toString();
    }
}
