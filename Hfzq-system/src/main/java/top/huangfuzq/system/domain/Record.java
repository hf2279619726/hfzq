package top.huangfuzq.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import top.huangfuzq.common.annotation.Excel;
import top.huangfuzq.common.core.domain.BaseEntity;

/**
 * 消费记录对象 record
 * 
 * @author hfzq
 * @date 2023-03-17
 */
public class Record extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 学号 */
    @Excel(name = "学号")
    private Long studentNumber;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 寝室 */
    @Excel(name = "寝室")
    private String dormitory;

    /** 设备编号 */
    @Excel(name = "设备编号")
    private String deviceNumber;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtCreated;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtModified;

    /** 用水时长 */
    @Excel(name = "用水时长")
    private String waterDuration;

    /** 用水量 */
    @Excel(name = "用水量")
    private String waterConsumption;

    /** 计费 */
    @Excel(name = "计费")
    private String charge;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStudentNumber(Long studentNumber) 
    {
        this.studentNumber = studentNumber;
    }

    public Long getStudentNumber() 
    {
        return studentNumber;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setDormitory(String dormitory) 
    {
        this.dormitory = dormitory;
    }

    public String getDormitory() 
    {
        return dormitory;
    }
    public void setDeviceNumber(String deviceNumber) 
    {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceNumber() 
    {
        return deviceNumber;
    }
    public void setGmtCreated(Date gmtCreated) 
    {
        this.gmtCreated = gmtCreated;
    }

    public Date getGmtCreated() 
    {
        return gmtCreated;
    }
    public void setGmtModified(Date gmtModified) 
    {
        this.gmtModified = gmtModified;
    }

    public Date getGmtModified() 
    {
        return gmtModified;
    }
    public void setWaterDuration(String waterDuration) 
    {
        this.waterDuration = waterDuration;
    }

    public String getWaterDuration() 
    {
        return waterDuration;
    }
    public void setWaterConsumption(String waterConsumption) 
    {
        this.waterConsumption = waterConsumption;
    }

    public String getWaterConsumption() 
    {
        return waterConsumption;
    }
    public void setCharge(String charge) 
    {
        this.charge = charge;
    }

    public String getCharge() 
    {
        return charge;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("studentNumber", getStudentNumber())
            .append("name", getName())
            .append("dormitory", getDormitory())
            .append("deviceNumber", getDeviceNumber())
            .append("gmtCreated", getGmtCreated())
            .append("gmtModified", getGmtModified())
            .append("waterDuration", getWaterDuration())
            .append("waterConsumption", getWaterConsumption())
            .append("charge", getCharge())
            .toString();
    }
}
