package top.huangfuzq.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import top.huangfuzq.common.annotation.Excel;
import top.huangfuzq.common.core.domain.BaseEntity;

/**
 * 学生管理对象 student
 * 
 * @author hfzq
 * @date 2023-03-17
 */
public class Student extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 学号 */
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 性别 */
    @Excel(name = "性别")
    private String gender;

    /** 分院 */
    @Excel(name = "分院")
    private String branchCourts;

    /** 班级 */
    @Excel(name = "班级")
    private String grade;

    /** 账户余额(元) */
    @Excel(name = "账户余额(元)")
    private String currentAccountBalance;

    /** 专业 */
    @Excel(name = "专业")
    private String major;

    /** 金额 */
    @Excel(name = "金额")
    private String money;

    /** 时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date date;

    /** 数据创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "数据创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtCreate;

    /** 数据最后修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "数据最后修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtModified;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setGender(String gender) 
    {
        this.gender = gender;
    }

    public String getGender() 
    {
        return gender;
    }
    public void setBranchCourts(String branchCourts) 
    {
        this.branchCourts = branchCourts;
    }

    public String getBranchCourts() 
    {
        return branchCourts;
    }
    public void setGrade(String grade) 
    {
        this.grade = grade;
    }

    public String getGrade() 
    {
        return grade;
    }
    public void setCurrentAccountBalance(String currentAccountBalance) 
    {
        this.currentAccountBalance = currentAccountBalance;
    }

    public String getCurrentAccountBalance() 
    {
        return currentAccountBalance;
    }
    public void setMajor(String major) 
    {
        this.major = major;
    }

    public String getMajor() 
    {
        return major;
    }
    public void setMoney(String money) 
    {
        this.money = money;
    }

    public String getMoney() 
    {
        return money;
    }
    public void setDate(Date date) 
    {
        this.date = date;
    }

    public Date getDate() 
    {
        return date;
    }
    public void setGmtCreate(Date gmtCreate) 
    {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtCreate() 
    {
        return gmtCreate;
    }
    public void setGmtModified(Date gmtModified) 
    {
        this.gmtModified = gmtModified;
    }

    public Date getGmtModified() 
    {
        return gmtModified;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("gender", getGender())
            .append("branchCourts", getBranchCourts())
            .append("grade", getGrade())
            .append("currentAccountBalance", getCurrentAccountBalance())
            .append("major", getMajor())
            .append("money", getMoney())
            .append("date", getDate())
            .append("gmtCreate", getGmtCreate())
            .append("gmtModified", getGmtModified())
            .toString();
    }
}
