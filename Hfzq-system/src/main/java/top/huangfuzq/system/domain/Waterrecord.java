package top.huangfuzq.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import top.huangfuzq.common.annotation.Excel;
import top.huangfuzq.common.core.domain.BaseEntity;

/**
 * 设备用水记录对象 waterrecord
 * 
 * @author hfzq
 * @date 2023-03-17
 */
public class Waterrecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据id */
    private Long id;

    /** 学号 */
    @Excel(name = "学号")
    private Long studentNumber;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 设备位置 */
    @Excel(name = "设备位置")
    private String equipmentLocation;

    /** 设备号 */
    @Excel(name = "设备号")
    private String placeholder;

    /** 用水时长 */
    @Excel(name = "用水时长")
    private String waterDuration;

    /** 用水量 */
    @Excel(name = "用水量")
    private Long waterConsumption;

    /** 计费 */
    @Excel(name = "计费")
    private String charge;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtCreate;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtModified;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStudentNumber(Long studentNumber) 
    {
        this.studentNumber = studentNumber;
    }

    public Long getStudentNumber() 
    {
        return studentNumber;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setEquipmentLocation(String equipmentLocation) 
    {
        this.equipmentLocation = equipmentLocation;
    }

    public String getEquipmentLocation() 
    {
        return equipmentLocation;
    }
    public void setPlaceholder(String placeholder) 
    {
        this.placeholder = placeholder;
    }

    public String getPlaceholder() 
    {
        return placeholder;
    }
    public void setWaterDuration(String waterDuration) 
    {
        this.waterDuration = waterDuration;
    }

    public String getWaterDuration() 
    {
        return waterDuration;
    }
    public void setWaterConsumption(Long waterConsumption) 
    {
        this.waterConsumption = waterConsumption;
    }

    public Long getWaterConsumption() 
    {
        return waterConsumption;
    }
    public void setCharge(String charge) 
    {
        this.charge = charge;
    }

    public String getCharge() 
    {
        return charge;
    }
    public void setGmtCreate(Date gmtCreate) 
    {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtCreate() 
    {
        return gmtCreate;
    }
    public void setGmtModified(Date gmtModified) 
    {
        this.gmtModified = gmtModified;
    }

    public Date getGmtModified() 
    {
        return gmtModified;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("studentNumber", getStudentNumber())
            .append("name", getName())
            .append("equipmentLocation", getEquipmentLocation())
            .append("placeholder", getPlaceholder())
            .append("waterDuration", getWaterDuration())
            .append("waterConsumption", getWaterConsumption())
            .append("charge", getCharge())
            .append("gmtCreate", getGmtCreate())
            .append("gmtModified", getGmtModified())
            .toString();
    }
}
