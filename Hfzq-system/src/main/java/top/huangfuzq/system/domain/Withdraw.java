package top.huangfuzq.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import top.huangfuzq.common.annotation.Excel;
import top.huangfuzq.common.core.domain.BaseEntity;

/**
 * 退款记录对象 withdraw
 * 
 * @author hfzq
 * @date 2023-03-17
 */
public class Withdraw extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据id */
    private Long id;

    /** 学号 */
    @Excel(name = "学号")
    private Long studentNumber;

    /** 楼栋号 */
    @Excel(name = "楼栋号")
    private String buildingPillarNumber;

    /** 寝室 */
    @Excel(name = "寝室")
    private String dormitory;

    /** 退款金额 */
    @Excel(name = "退款金额")
    private String refund;

    /** 退款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "退款时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date refundTime;

    /** 操作人 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "操作人", width = 30, dateFormat = "yyyy-MM-dd")
    private Date operator;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStudentNumber(Long studentNumber) 
    {
        this.studentNumber = studentNumber;
    }

    public Long getStudentNumber() 
    {
        return studentNumber;
    }
    public void setBuildingPillarNumber(String buildingPillarNumber) 
    {
        this.buildingPillarNumber = buildingPillarNumber;
    }

    public String getBuildingPillarNumber() 
    {
        return buildingPillarNumber;
    }
    public void setDormitory(String dormitory) 
    {
        this.dormitory = dormitory;
    }

    public String getDormitory() 
    {
        return dormitory;
    }
    public void setRefund(String refund) 
    {
        this.refund = refund;
    }

    public String getRefund() 
    {
        return refund;
    }
    public void setRefundTime(Date refundTime) 
    {
        this.refundTime = refundTime;
    }

    public Date getRefundTime() 
    {
        return refundTime;
    }
    public void setOperator(Date operator) 
    {
        this.operator = operator;
    }

    public Date getOperator() 
    {
        return operator;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("studentNumber", getStudentNumber())
            .append("buildingPillarNumber", getBuildingPillarNumber())
            .append("dormitory", getDormitory())
            .append("refund", getRefund())
            .append("refundTime", getRefundTime())
            .append("operator", getOperator())
            .toString();
    }
}
