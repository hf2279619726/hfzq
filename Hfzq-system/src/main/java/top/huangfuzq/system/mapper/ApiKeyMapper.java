package top.huangfuzq.system.mapper;

import java.util.List;
import top.huangfuzq.system.domain.ApiKey;

/**
 * 用于存储API键信息的数据Mapper接口
 * 
 * @author hfzq
 * @date 2023-03-17
 */
public interface ApiKeyMapper 
{
    /**
     * 查询用于存储API键信息的数据
     * 
     * @param id 用于存储API键信息的数据主键
     * @return 用于存储API键信息的数据
     */
    public ApiKey selectApiKeyById(Long id);

    /**
     * 查询用于存储API键信息的数据列表
     * 
     * @param apiKey 用于存储API键信息的数据
     * @return 用于存储API键信息的数据集合
     */
    public List<ApiKey> selectApiKeyList(ApiKey apiKey);

    /**
     * 新增用于存储API键信息的数据
     * 
     * @param apiKey 用于存储API键信息的数据
     * @return 结果
     */
    public int insertApiKey(ApiKey apiKey);

    /**
     * 修改用于存储API键信息的数据
     * 
     * @param apiKey 用于存储API键信息的数据
     * @return 结果
     */
    public int updateApiKey(ApiKey apiKey);

    /**
     * 删除用于存储API键信息的数据
     * 
     * @param id 用于存储API键信息的数据主键
     * @return 结果
     */
    public int deleteApiKeyById(Long id);

    /**
     * 批量删除用于存储API键信息的数据
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteApiKeyByIds(Long[] ids);
}
