package top.huangfuzq.system.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;
import top.huangfuzq.system.domain.DeviceProfiles;

/**
 * 存储设备概要文件的详细信息Mapper接口
 *
 * @author hfzq
 * @date 2023-03-17
 */
@Repository
public interface DeviceProfilesMapper
{
    /**
     * 查询存储设备概要文件的详细信息
     *
     * @param id 存储设备概要文件的详细信息主键
     * @return 存储设备概要文件的详细信息
     */
    public DeviceProfiles selectDeviceProfilesById(Long id);

    /**
     * 查询存储设备概要文件的详细信息列表
     *
     * @param deviceProfiles 存储设备概要文件的详细信息
     * @return 存储设备概要文件的详细信息集合
     */
    public List<DeviceProfiles> selectDeviceProfilesList(DeviceProfiles deviceProfiles);

    /**
     * 新增存储设备概要文件的详细信息
     *
     * @param deviceProfiles 存储设备概要文件的详细信息
     * @return 结果
     */
    public int insertDeviceProfiles(DeviceProfiles deviceProfiles);

    /**
     * 修改存储设备概要文件的详细信息
     *
     * @param deviceProfiles 存储设备概要文件的详细信息
     * @return 结果
     */
    public int updateDeviceProfiles(DeviceProfiles deviceProfiles);

    /**
     * 删除存储设备概要文件的详细信息
     *
     * @param id 存储设备概要文件的详细信息主键
     * @return 结果
     */
    public int deleteDeviceProfilesById(Long id);

    /**
     * 批量删除存储设备概要文件的详细信息
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDeviceProfilesByIds(Long[] ids);
}
