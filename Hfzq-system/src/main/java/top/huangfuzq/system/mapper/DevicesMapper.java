package top.huangfuzq.system.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;
import top.huangfuzq.system.domain.Devices;

/**
 * 设备管理Mapper接口
 *
 * @author hfzq
 * @date 2023-03-17
 */
@Repository
public interface DevicesMapper
{
    /**
     * 查询设备管理
     *
     * @param id 设备管理主键
     * @return 设备管理
     */
    public Devices selectDevicesById(Long id);

    /**
     * 查询设备管理列表
     *
     * @param devices 设备管理
     * @return 设备管理集合
     */
    public List<Devices> selectDevicesList(Devices devices);

    /**
     * 新增设备管理
     *
     * @param devices 设备管理
     * @return 结果
     */
    public int insertDevices(Devices devices);

    /**
     * 修改设备管理
     *
     * @param devices 设备管理
     * @return 结果
     */
    public int updateDevices(Devices devices);

    /**
     * 删除设备管理
     *
     * @param id 设备管理主键
     * @return 结果
     */
    public int deleteDevicesById(Long id);

    /**
     * 批量删除设备管理
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDevicesByIds(Long[] ids);
}
