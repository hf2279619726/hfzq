package top.huangfuzq.system.mapper;

import java.util.List;
import top.huangfuzq.system.domain.GatewayProfile;

/**
 * 网关概要配置Mapper接口
 * 
 * @author hfzq
 * @date 2023-03-17
 */
public interface GatewayProfileMapper 
{
    /**
     * 查询网关概要配置
     * 
     * @param id 网关概要配置主键
     * @return 网关概要配置
     */
    public GatewayProfile selectGatewayProfileById(Long id);

    /**
     * 查询网关概要配置列表
     * 
     * @param gatewayProfile 网关概要配置
     * @return 网关概要配置集合
     */
    public List<GatewayProfile> selectGatewayProfileList(GatewayProfile gatewayProfile);

    /**
     * 新增网关概要配置
     * 
     * @param gatewayProfile 网关概要配置
     * @return 结果
     */
    public int insertGatewayProfile(GatewayProfile gatewayProfile);

    /**
     * 修改网关概要配置
     * 
     * @param gatewayProfile 网关概要配置
     * @return 结果
     */
    public int updateGatewayProfile(GatewayProfile gatewayProfile);

    /**
     * 删除网关概要配置
     * 
     * @param id 网关概要配置主键
     * @return 结果
     */
    public int deleteGatewayProfileById(Long id);

    /**
     * 批量删除网关概要配置
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteGatewayProfileByIds(Long[] ids);
}
