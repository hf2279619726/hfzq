package top.huangfuzq.system.mapper;

import java.util.List;
import top.huangfuzq.system.domain.Withdraw;

/**
 * 退款记录Mapper接口
 * 
 * @author hfzq
 * @date 2023-03-17
 */
public interface WithdrawMapper 
{
    /**
     * 查询退款记录
     * 
     * @param id 退款记录主键
     * @return 退款记录
     */
    public Withdraw selectWithdrawById(Long id);

    /**
     * 查询退款记录列表
     * 
     * @param withdraw 退款记录
     * @return 退款记录集合
     */
    public List<Withdraw> selectWithdrawList(Withdraw withdraw);

    /**
     * 新增退款记录
     * 
     * @param withdraw 退款记录
     * @return 结果
     */
    public int insertWithdraw(Withdraw withdraw);

    /**
     * 修改退款记录
     * 
     * @param withdraw 退款记录
     * @return 结果
     */
    public int updateWithdraw(Withdraw withdraw);

    /**
     * 删除退款记录
     * 
     * @param id 退款记录主键
     * @return 结果
     */
    public int deleteWithdrawById(Long id);

    /**
     * 批量删除退款记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWithdrawByIds(Long[] ids);
}
