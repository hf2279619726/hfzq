package top.huangfuzq.system.service;

import java.util.List;
import top.huangfuzq.system.domain.Fuotadeployment;

/**
 * 无线固件更新部署Service接口
 * 
 * @author hfzq
 * @date 2023-03-17
 */
public interface IFuotadeploymentService 
{
    /**
     * 查询无线固件更新部署
     * 
     * @param id 无线固件更新部署主键
     * @return 无线固件更新部署
     */
    public Fuotadeployment selectFuotadeploymentById(Long id);

    /**
     * 查询无线固件更新部署列表
     * 
     * @param fuotadeployment 无线固件更新部署
     * @return 无线固件更新部署集合
     */
    public List<Fuotadeployment> selectFuotadeploymentList(Fuotadeployment fuotadeployment);

    /**
     * 新增无线固件更新部署
     * 
     * @param fuotadeployment 无线固件更新部署
     * @return 结果
     */
    public int insertFuotadeployment(Fuotadeployment fuotadeployment);

    /**
     * 修改无线固件更新部署
     * 
     * @param fuotadeployment 无线固件更新部署
     * @return 结果
     */
    public int updateFuotadeployment(Fuotadeployment fuotadeployment);

    /**
     * 批量删除无线固件更新部署
     * 
     * @param ids 需要删除的无线固件更新部署主键集合
     * @return 结果
     */
    public int deleteFuotadeploymentByIds(Long[] ids);

    /**
     * 删除无线固件更新部署信息
     * 
     * @param id 无线固件更新部署主键
     * @return 结果
     */
    public int deleteFuotadeploymentById(Long id);
}
