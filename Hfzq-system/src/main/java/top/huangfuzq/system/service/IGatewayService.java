package top.huangfuzq.system.service;

import java.util.List;
import top.huangfuzq.system.domain.Gateway;

/**
 * 网关信息Service接口
 * 
 * @author hfzq
 * @date 2023-03-17
 */
public interface IGatewayService 
{
    /**
     * 查询网关信息
     * 
     * @param id 网关信息主键
     * @return 网关信息
     */
    public Gateway selectGatewayById(Long id);

    /**
     * 查询网关信息列表
     * 
     * @param gateway 网关信息
     * @return 网关信息集合
     */
    public List<Gateway> selectGatewayList(Gateway gateway);

    /**
     * 新增网关信息
     * 
     * @param gateway 网关信息
     * @return 结果
     */
    public int insertGateway(Gateway gateway);

    /**
     * 修改网关信息
     * 
     * @param gateway 网关信息
     * @return 结果
     */
    public int updateGateway(Gateway gateway);

    /**
     * 批量删除网关信息
     * 
     * @param ids 需要删除的网关信息主键集合
     * @return 结果
     */
    public int deleteGatewayByIds(Long[] ids);

    /**
     * 删除网关信息信息
     * 
     * @param id 网关信息主键
     * @return 结果
     */
    public int deleteGatewayById(Long id);
}
