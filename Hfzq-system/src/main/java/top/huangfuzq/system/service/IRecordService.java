package top.huangfuzq.system.service;

import java.util.List;
import top.huangfuzq.system.domain.Record;

/**
 * 消费记录Service接口
 * 
 * @author hfzq
 * @date 2023-03-17
 */
public interface IRecordService 
{
    /**
     * 查询消费记录
     * 
     * @param id 消费记录主键
     * @return 消费记录
     */
    public Record selectRecordById(Long id);

    /**
     * 查询消费记录列表
     * 
     * @param record 消费记录
     * @return 消费记录集合
     */
    public List<Record> selectRecordList(Record record);

    /**
     * 新增消费记录
     * 
     * @param record 消费记录
     * @return 结果
     */
    public int insertRecord(Record record);

    /**
     * 修改消费记录
     * 
     * @param record 消费记录
     * @return 结果
     */
    public int updateRecord(Record record);

    /**
     * 批量删除消费记录
     * 
     * @param ids 需要删除的消费记录主键集合
     * @return 结果
     */
    public int deleteRecordByIds(Long[] ids);

    /**
     * 删除消费记录信息
     * 
     * @param id 消费记录主键
     * @return 结果
     */
    public int deleteRecordById(Long id);
}
