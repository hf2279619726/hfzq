package top.huangfuzq.system.service;

import java.util.List;
import top.huangfuzq.system.domain.Waterrecord;

/**
 * 设备用水记录Service接口
 * 
 * @author hfzq
 * @date 2023-03-17
 */
public interface IWaterrecordService 
{
    /**
     * 查询设备用水记录
     * 
     * @param id 设备用水记录主键
     * @return 设备用水记录
     */
    public Waterrecord selectWaterrecordById(Long id);

    /**
     * 查询设备用水记录列表
     * 
     * @param waterrecord 设备用水记录
     * @return 设备用水记录集合
     */
    public List<Waterrecord> selectWaterrecordList(Waterrecord waterrecord);

    /**
     * 新增设备用水记录
     * 
     * @param waterrecord 设备用水记录
     * @return 结果
     */
    public int insertWaterrecord(Waterrecord waterrecord);

    /**
     * 修改设备用水记录
     * 
     * @param waterrecord 设备用水记录
     * @return 结果
     */
    public int updateWaterrecord(Waterrecord waterrecord);

    /**
     * 批量删除设备用水记录
     * 
     * @param ids 需要删除的设备用水记录主键集合
     * @return 结果
     */
    public int deleteWaterrecordByIds(Long[] ids);

    /**
     * 删除设备用水记录信息
     * 
     * @param id 设备用水记录主键
     * @return 结果
     */
    public int deleteWaterrecordById(Long id);
}
