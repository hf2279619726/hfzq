package top.huangfuzq.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.huangfuzq.system.mapper.ApiKeyMapper;
import top.huangfuzq.system.domain.ApiKey;
import top.huangfuzq.system.service.IApiKeyService;

/**
 * 用于存储API键信息的数据Service业务层处理
 * 
 * @author hfzq
 * @date 2023-03-17
 */
@Service
public class ApiKeyServiceImpl implements IApiKeyService 
{
    @Autowired
    private ApiKeyMapper apiKeyMapper;

    /**
     * 查询用于存储API键信息的数据
     * 
     * @param id 用于存储API键信息的数据主键
     * @return 用于存储API键信息的数据
     */
    @Override
    public ApiKey selectApiKeyById(Long id)
    {
        return apiKeyMapper.selectApiKeyById(id);
    }

    /**
     * 查询用于存储API键信息的数据列表
     * 
     * @param apiKey 用于存储API键信息的数据
     * @return 用于存储API键信息的数据
     */
    @Override
    public List<ApiKey> selectApiKeyList(ApiKey apiKey)
    {
        return apiKeyMapper.selectApiKeyList(apiKey);
    }

    /**
     * 新增用于存储API键信息的数据
     * 
     * @param apiKey 用于存储API键信息的数据
     * @return 结果
     */
    @Override
    public int insertApiKey(ApiKey apiKey)
    {
        return apiKeyMapper.insertApiKey(apiKey);
    }

    /**
     * 修改用于存储API键信息的数据
     * 
     * @param apiKey 用于存储API键信息的数据
     * @return 结果
     */
    @Override
    public int updateApiKey(ApiKey apiKey)
    {
        return apiKeyMapper.updateApiKey(apiKey);
    }

    /**
     * 批量删除用于存储API键信息的数据
     * 
     * @param ids 需要删除的用于存储API键信息的数据主键
     * @return 结果
     */
    @Override
    public int deleteApiKeyByIds(Long[] ids)
    {
        return apiKeyMapper.deleteApiKeyByIds(ids);
    }

    /**
     * 删除用于存储API键信息的数据信息
     * 
     * @param id 用于存储API键信息的数据主键
     * @return 结果
     */
    @Override
    public int deleteApiKeyById(Long id)
    {
        return apiKeyMapper.deleteApiKeyById(id);
    }
}
