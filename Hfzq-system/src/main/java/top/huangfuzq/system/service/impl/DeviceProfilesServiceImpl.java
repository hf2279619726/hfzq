package top.huangfuzq.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.huangfuzq.system.mapper.DeviceProfilesMapper;
import top.huangfuzq.system.domain.DeviceProfiles;
import top.huangfuzq.system.service.IDeviceProfilesService;

/**
 * 存储设备概要文件的详细信息Service业务层处理
 * 
 * @author hfzq
 * @date 2023-03-17
 */
@Service
public class DeviceProfilesServiceImpl implements IDeviceProfilesService 
{
    @Autowired
    private DeviceProfilesMapper deviceProfilesMapper;

    /**
     * 查询存储设备概要文件的详细信息
     * 
     * @param id 存储设备概要文件的详细信息主键
     * @return 存储设备概要文件的详细信息
     */
    @Override
    public DeviceProfiles selectDeviceProfilesById(Long id)
    {
        return deviceProfilesMapper.selectDeviceProfilesById(id);
    }

    /**
     * 查询存储设备概要文件的详细信息列表
     * 
     * @param deviceProfiles 存储设备概要文件的详细信息
     * @return 存储设备概要文件的详细信息
     */
    @Override
    public List<DeviceProfiles> selectDeviceProfilesList(DeviceProfiles deviceProfiles)
    {
        return deviceProfilesMapper.selectDeviceProfilesList(deviceProfiles);
    }

    /**
     * 新增存储设备概要文件的详细信息
     * 
     * @param deviceProfiles 存储设备概要文件的详细信息
     * @return 结果
     */
    @Override
    public int insertDeviceProfiles(DeviceProfiles deviceProfiles)
    {
        return deviceProfilesMapper.insertDeviceProfiles(deviceProfiles);
    }

    /**
     * 修改存储设备概要文件的详细信息
     * 
     * @param deviceProfiles 存储设备概要文件的详细信息
     * @return 结果
     */
    @Override
    public int updateDeviceProfiles(DeviceProfiles deviceProfiles)
    {
        return deviceProfilesMapper.updateDeviceProfiles(deviceProfiles);
    }

    /**
     * 批量删除存储设备概要文件的详细信息
     * 
     * @param ids 需要删除的存储设备概要文件的详细信息主键
     * @return 结果
     */
    @Override
    public int deleteDeviceProfilesByIds(Long[] ids)
    {
        return deviceProfilesMapper.deleteDeviceProfilesByIds(ids);
    }

    /**
     * 删除存储设备概要文件的详细信息信息
     * 
     * @param id 存储设备概要文件的详细信息主键
     * @return 结果
     */
    @Override
    public int deleteDeviceProfilesById(Long id)
    {
        return deviceProfilesMapper.deleteDeviceProfilesById(id);
    }
}
