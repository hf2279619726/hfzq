package top.huangfuzq.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.huangfuzq.system.mapper.FuotadeploymentMapper;
import top.huangfuzq.system.domain.Fuotadeployment;
import top.huangfuzq.system.service.IFuotadeploymentService;

/**
 * 无线固件更新部署Service业务层处理
 * 
 * @author hfzq
 * @date 2023-03-17
 */
@Service
public class FuotadeploymentServiceImpl implements IFuotadeploymentService 
{
    @Autowired
    private FuotadeploymentMapper fuotadeploymentMapper;

    /**
     * 查询无线固件更新部署
     * 
     * @param id 无线固件更新部署主键
     * @return 无线固件更新部署
     */
    @Override
    public Fuotadeployment selectFuotadeploymentById(Long id)
    {
        return fuotadeploymentMapper.selectFuotadeploymentById(id);
    }

    /**
     * 查询无线固件更新部署列表
     * 
     * @param fuotadeployment 无线固件更新部署
     * @return 无线固件更新部署
     */
    @Override
    public List<Fuotadeployment> selectFuotadeploymentList(Fuotadeployment fuotadeployment)
    {
        return fuotadeploymentMapper.selectFuotadeploymentList(fuotadeployment);
    }

    /**
     * 新增无线固件更新部署
     * 
     * @param fuotadeployment 无线固件更新部署
     * @return 结果
     */
    @Override
    public int insertFuotadeployment(Fuotadeployment fuotadeployment)
    {
        return fuotadeploymentMapper.insertFuotadeployment(fuotadeployment);
    }

    /**
     * 修改无线固件更新部署
     * 
     * @param fuotadeployment 无线固件更新部署
     * @return 结果
     */
    @Override
    public int updateFuotadeployment(Fuotadeployment fuotadeployment)
    {
        return fuotadeploymentMapper.updateFuotadeployment(fuotadeployment);
    }

    /**
     * 批量删除无线固件更新部署
     * 
     * @param ids 需要删除的无线固件更新部署主键
     * @return 结果
     */
    @Override
    public int deleteFuotadeploymentByIds(Long[] ids)
    {
        return fuotadeploymentMapper.deleteFuotadeploymentByIds(ids);
    }

    /**
     * 删除无线固件更新部署信息
     * 
     * @param id 无线固件更新部署主键
     * @return 结果
     */
    @Override
    public int deleteFuotadeploymentById(Long id)
    {
        return fuotadeploymentMapper.deleteFuotadeploymentById(id);
    }
}
