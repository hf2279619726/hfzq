package top.huangfuzq.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.huangfuzq.system.mapper.GatewayProfileMapper;
import top.huangfuzq.system.domain.GatewayProfile;
import top.huangfuzq.system.service.IGatewayProfileService;

/**
 * 网关概要配置Service业务层处理
 * 
 * @author hfzq
 * @date 2023-03-17
 */
@Service
public class GatewayProfileServiceImpl implements IGatewayProfileService 
{
    @Autowired
    private GatewayProfileMapper gatewayProfileMapper;

    /**
     * 查询网关概要配置
     * 
     * @param id 网关概要配置主键
     * @return 网关概要配置
     */
    @Override
    public GatewayProfile selectGatewayProfileById(Long id)
    {
        return gatewayProfileMapper.selectGatewayProfileById(id);
    }

    /**
     * 查询网关概要配置列表
     * 
     * @param gatewayProfile 网关概要配置
     * @return 网关概要配置
     */
    @Override
    public List<GatewayProfile> selectGatewayProfileList(GatewayProfile gatewayProfile)
    {
        return gatewayProfileMapper.selectGatewayProfileList(gatewayProfile);
    }

    /**
     * 新增网关概要配置
     * 
     * @param gatewayProfile 网关概要配置
     * @return 结果
     */
    @Override
    public int insertGatewayProfile(GatewayProfile gatewayProfile)
    {
        return gatewayProfileMapper.insertGatewayProfile(gatewayProfile);
    }

    /**
     * 修改网关概要配置
     * 
     * @param gatewayProfile 网关概要配置
     * @return 结果
     */
    @Override
    public int updateGatewayProfile(GatewayProfile gatewayProfile)
    {
        return gatewayProfileMapper.updateGatewayProfile(gatewayProfile);
    }

    /**
     * 批量删除网关概要配置
     * 
     * @param ids 需要删除的网关概要配置主键
     * @return 结果
     */
    @Override
    public int deleteGatewayProfileByIds(Long[] ids)
    {
        return gatewayProfileMapper.deleteGatewayProfileByIds(ids);
    }

    /**
     * 删除网关概要配置信息
     * 
     * @param id 网关概要配置主键
     * @return 结果
     */
    @Override
    public int deleteGatewayProfileById(Long id)
    {
        return gatewayProfileMapper.deleteGatewayProfileById(id);
    }
}
