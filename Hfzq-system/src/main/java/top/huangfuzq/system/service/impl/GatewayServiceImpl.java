package top.huangfuzq.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.huangfuzq.system.mapper.GatewayMapper;
import top.huangfuzq.system.domain.Gateway;
import top.huangfuzq.system.service.IGatewayService;

/**
 * 网关信息Service业务层处理
 * 
 * @author hfzq
 * @date 2023-03-17
 */
@Service
public class GatewayServiceImpl implements IGatewayService 
{
    @Autowired
    private GatewayMapper gatewayMapper;

    /**
     * 查询网关信息
     * 
     * @param id 网关信息主键
     * @return 网关信息
     */
    @Override
    public Gateway selectGatewayById(Long id)
    {
        return gatewayMapper.selectGatewayById(id);
    }

    /**
     * 查询网关信息列表
     * 
     * @param gateway 网关信息
     * @return 网关信息
     */
    @Override
    public List<Gateway> selectGatewayList(Gateway gateway)
    {
        return gatewayMapper.selectGatewayList(gateway);
    }

    /**
     * 新增网关信息
     * 
     * @param gateway 网关信息
     * @return 结果
     */
    @Override
    public int insertGateway(Gateway gateway)
    {
        return gatewayMapper.insertGateway(gateway);
    }

    /**
     * 修改网关信息
     * 
     * @param gateway 网关信息
     * @return 结果
     */
    @Override
    public int updateGateway(Gateway gateway)
    {
        return gatewayMapper.updateGateway(gateway);
    }

    /**
     * 批量删除网关信息
     * 
     * @param ids 需要删除的网关信息主键
     * @return 结果
     */
    @Override
    public int deleteGatewayByIds(Long[] ids)
    {
        return gatewayMapper.deleteGatewayByIds(ids);
    }

    /**
     * 删除网关信息信息
     * 
     * @param id 网关信息主键
     * @return 结果
     */
    @Override
    public int deleteGatewayById(Long id)
    {
        return gatewayMapper.deleteGatewayById(id);
    }
}
