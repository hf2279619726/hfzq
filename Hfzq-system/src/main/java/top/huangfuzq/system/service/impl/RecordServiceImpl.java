package top.huangfuzq.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.huangfuzq.system.mapper.RecordMapper;
import top.huangfuzq.system.domain.Record;
import top.huangfuzq.system.service.IRecordService;

/**
 * 消费记录Service业务层处理
 * 
 * @author hfzq
 * @date 2023-03-17
 */
@Service
public class RecordServiceImpl implements IRecordService 
{
    @Autowired
    private RecordMapper recordMapper;

    /**
     * 查询消费记录
     * 
     * @param id 消费记录主键
     * @return 消费记录
     */
    @Override
    public Record selectRecordById(Long id)
    {
        return recordMapper.selectRecordById(id);
    }

    /**
     * 查询消费记录列表
     * 
     * @param record 消费记录
     * @return 消费记录
     */
    @Override
    public List<Record> selectRecordList(Record record)
    {
        return recordMapper.selectRecordList(record);
    }

    /**
     * 新增消费记录
     * 
     * @param record 消费记录
     * @return 结果
     */
    @Override
    public int insertRecord(Record record)
    {
        return recordMapper.insertRecord(record);
    }

    /**
     * 修改消费记录
     * 
     * @param record 消费记录
     * @return 结果
     */
    @Override
    public int updateRecord(Record record)
    {
        return recordMapper.updateRecord(record);
    }

    /**
     * 批量删除消费记录
     * 
     * @param ids 需要删除的消费记录主键
     * @return 结果
     */
    @Override
    public int deleteRecordByIds(Long[] ids)
    {
        return recordMapper.deleteRecordByIds(ids);
    }

    /**
     * 删除消费记录信息
     * 
     * @param id 消费记录主键
     * @return 结果
     */
    @Override
    public int deleteRecordById(Long id)
    {
        return recordMapper.deleteRecordById(id);
    }
}
