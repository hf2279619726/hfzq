package top.huangfuzq.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.huangfuzq.system.mapper.WaterrecordMapper;
import top.huangfuzq.system.domain.Waterrecord;
import top.huangfuzq.system.service.IWaterrecordService;

/**
 * 设备用水记录Service业务层处理
 * 
 * @author hfzq
 * @date 2023-03-17
 */
@Service
public class WaterrecordServiceImpl implements IWaterrecordService 
{
    @Autowired
    private WaterrecordMapper waterrecordMapper;

    /**
     * 查询设备用水记录
     * 
     * @param id 设备用水记录主键
     * @return 设备用水记录
     */
    @Override
    public Waterrecord selectWaterrecordById(Long id)
    {
        return waterrecordMapper.selectWaterrecordById(id);
    }

    /**
     * 查询设备用水记录列表
     * 
     * @param waterrecord 设备用水记录
     * @return 设备用水记录
     */
    @Override
    public List<Waterrecord> selectWaterrecordList(Waterrecord waterrecord)
    {
        return waterrecordMapper.selectWaterrecordList(waterrecord);
    }

    /**
     * 新增设备用水记录
     * 
     * @param waterrecord 设备用水记录
     * @return 结果
     */
    @Override
    public int insertWaterrecord(Waterrecord waterrecord)
    {
        return waterrecordMapper.insertWaterrecord(waterrecord);
    }

    /**
     * 修改设备用水记录
     * 
     * @param waterrecord 设备用水记录
     * @return 结果
     */
    @Override
    public int updateWaterrecord(Waterrecord waterrecord)
    {
        return waterrecordMapper.updateWaterrecord(waterrecord);
    }

    /**
     * 批量删除设备用水记录
     * 
     * @param ids 需要删除的设备用水记录主键
     * @return 结果
     */
    @Override
    public int deleteWaterrecordByIds(Long[] ids)
    {
        return waterrecordMapper.deleteWaterrecordByIds(ids);
    }

    /**
     * 删除设备用水记录信息
     * 
     * @param id 设备用水记录主键
     * @return 结果
     */
    @Override
    public int deleteWaterrecordById(Long id)
    {
        return waterrecordMapper.deleteWaterrecordById(id);
    }
}
