package top.huangfuzq.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.huangfuzq.system.mapper.WithdrawMapper;
import top.huangfuzq.system.domain.Withdraw;
import top.huangfuzq.system.service.IWithdrawService;

/**
 * 退款记录Service业务层处理
 * 
 * @author hfzq
 * @date 2023-03-17
 */
@Service
public class WithdrawServiceImpl implements IWithdrawService 
{
    @Autowired
    private WithdrawMapper withdrawMapper;

    /**
     * 查询退款记录
     * 
     * @param id 退款记录主键
     * @return 退款记录
     */
    @Override
    public Withdraw selectWithdrawById(Long id)
    {
        return withdrawMapper.selectWithdrawById(id);
    }

    /**
     * 查询退款记录列表
     * 
     * @param withdraw 退款记录
     * @return 退款记录
     */
    @Override
    public List<Withdraw> selectWithdrawList(Withdraw withdraw)
    {
        return withdrawMapper.selectWithdrawList(withdraw);
    }

    /**
     * 新增退款记录
     * 
     * @param withdraw 退款记录
     * @return 结果
     */
    @Override
    public int insertWithdraw(Withdraw withdraw)
    {
        return withdrawMapper.insertWithdraw(withdraw);
    }

    /**
     * 修改退款记录
     * 
     * @param withdraw 退款记录
     * @return 结果
     */
    @Override
    public int updateWithdraw(Withdraw withdraw)
    {
        return withdrawMapper.updateWithdraw(withdraw);
    }

    /**
     * 批量删除退款记录
     * 
     * @param ids 需要删除的退款记录主键
     * @return 结果
     */
    @Override
    public int deleteWithdrawByIds(Long[] ids)
    {
        return withdrawMapper.deleteWithdrawByIds(ids);
    }

    /**
     * 删除退款记录信息
     * 
     * @param id 退款记录主键
     * @return 结果
     */
    @Override
    public int deleteWithdrawById(Long id)
    {
        return withdrawMapper.deleteWithdrawById(id);
    }
}
