import request from '@/utils/request'

// 查询无线固件更新部署列表
export function listFuotadeployment(query) {
  return request({
    url: '/system/fuotadeployment/list',
    method: 'get',
    params: query
  })
}

// 查询无线固件更新部署详细
export function getFuotadeployment(id) {
  return request({
    url: '/system/fuotadeployment/' + id,
    method: 'get'
  })
}

// 新增无线固件更新部署
export function addFuotadeployment(data) {
  return request({
    url: '/system/fuotadeployment',
    method: 'post',
    data: data
  })
}

// 修改无线固件更新部署
export function updateFuotadeployment(data) {
  return request({
    url: '/system/fuotadeployment',
    method: 'put',
    data: data
  })
}

// 删除无线固件更新部署
export function delFuotadeployment(id) {
  return request({
    url: '/system/fuotadeployment/' + id,
    method: 'delete'
  })
}
