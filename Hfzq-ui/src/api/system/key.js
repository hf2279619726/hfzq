import request from '@/utils/request'

// 查询用于存储API键信息的数据列表
export function listKey(query) {
  return request({
    url: '/system/key/list',
    method: 'get',
    params: query
  })
}

// 查询用于存储API键信息的数据详细
export function getKey(id) {
  return request({
    url: '/system/key/' + id,
    method: 'get'
  })
}

// 新增用于存储API键信息的数据
export function addKey(data) {
  return request({
    url: '/system/key',
    method: 'post',
    data: data
  })
}

// 修改用于存储API键信息的数据
export function updateKey(data) {
  return request({
    url: '/system/key',
    method: 'put',
    data: data
  })
}

// 删除用于存储API键信息的数据
export function delKey(id) {
  return request({
    url: '/system/key/' + id,
    method: 'delete'
  })
}
