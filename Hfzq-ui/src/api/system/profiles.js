import request from '@/utils/request'

// 查询存储设备概要文件的详细信息列表
export function listProfiles(query) {
  return request({
    url: '/system/profiles/list',
    method: 'get',
    params: query
  })
}

// 查询存储设备概要文件的详细信息详细
export function getProfiles(id) {
  return request({
    url: '/system/profiles/' + id,
    method: 'get'
  })
}

// 新增存储设备概要文件的详细信息
export function addProfiles(data) {
  return request({
    url: '/system/profiles',
    method: 'post',
    data: data
  })
}

// 修改存储设备概要文件的详细信息
export function updateProfiles(data) {
  return request({
    url: '/system/profiles',
    method: 'put',
    data: data
  })
}

// 删除存储设备概要文件的详细信息
export function delProfiles(id) {
  return request({
    url: '/system/profiles/' + id,
    method: 'delete'
  })
}
