import request from '@/utils/request'

// 查询设备用水记录列表
export function listWaterrecord(query) {
  return request({
    url: '/system/waterrecord/list',
    method: 'get',
    params: query
  })
}

// 查询设备用水记录详细
export function getWaterrecord(id) {
  return request({
    url: '/system/waterrecord/' + id,
    method: 'get'
  })
}

// 新增设备用水记录
export function addWaterrecord(data) {
  return request({
    url: '/system/waterrecord',
    method: 'post',
    data: data
  })
}

// 修改设备用水记录
export function updateWaterrecord(data) {
  return request({
    url: '/system/waterrecord',
    method: 'put',
    data: data
  })
}

// 删除设备用水记录
export function delWaterrecord(id) {
  return request({
    url: '/system/waterrecord/' + id,
    method: 'delete'
  })
}
