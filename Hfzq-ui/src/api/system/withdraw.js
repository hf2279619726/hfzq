import request from '@/utils/request'

// 查询退款记录列表
export function listWithdraw(query) {
  return request({
    url: '/system/withdraw/list',
    method: 'get',
    params: query
  })
}

// 查询退款记录详细
export function getWithdraw(id) {
  return request({
    url: '/system/withdraw/' + id,
    method: 'get'
  })
}

// 新增退款记录
export function addWithdraw(data) {
  return request({
    url: '/system/withdraw',
    method: 'post',
    data: data
  })
}

// 修改退款记录
export function updateWithdraw(data) {
  return request({
    url: '/system/withdraw',
    method: 'put',
    data: data
  })
}

// 删除退款记录
export function delWithdraw(id) {
  return request({
    url: '/system/withdraw/' + id,
    method: 'delete'
  })
}
